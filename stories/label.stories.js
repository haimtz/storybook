import { storiesOf } from '@storybook/vue';
import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all.js'

import SysLabel from '../src/components/SysLabel';

const text = 'this is label';
const icon_text = 'this is a label with text';
const style = { color: '#d14040' };
const icon_style = "fas fa-at";

storiesOf('Label', module)
    .add('display text only', () => ({
        components: { SysLabel },
        template: '<sys-label :text="text"></sys-label>',
        data: () => ({ text })
    })).add('display text with style', () => ({
        components: { SysLabel },
        template: '<sys-label :text="text" :labelStyle="style"></sys-label>',
        data: () => ({ text, style })
    }))
    .add('label with icon', () => ({
        components: { SysLabel },
        template: '<sys-label :text="icon_text" :labelStyle="style"><i :class="icon_style"></i></sys-label>',
        data: () => ({ icon_text, icon_style, style })
    }));