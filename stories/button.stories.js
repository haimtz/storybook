/* eslint-disable react/react-in-jsx-scope, react/no-this-in-sfc */

import { storiesOf } from '@storybook/vue';
import { action } from '@storybook/addon-actions';
import 'bootstrap/dist/css/bootstrap.css';
import add_icon from '../src/assets/add_target_icon.png';

import ImageButton from "../src/components/ImageButton"

const text = "click me";
const classname = "btn btn-primary";
const style = {
  textAlign: 'center',
  background: 'transparent',
  border: 'none',
  width: '63px'
};

storiesOf('Button', module)
  .add('button regular', () => ({
    components: { ImageButton },
    template: '<image-button :text="text" @click="onclick"></image-button>',
    data: () => ({ text }),
    methods: {
      onclick: action(event)
    }
  })).
  add('button with classname', () => ({
    components: { ImageButton },
    template: '<image-button :text="text" :classname="classname" @click="onclick"></image-button>',
    data: () => ({ text, classname }),
    methods: { onclick: action(event) }
  })).add('button with image', () => ({
    components: { ImageButton },
    template: '<image-button :text="text" :buttonStyle="style" @click="onclick"><img :src="add_icon"></image-button>',
    data: () => ({ add_icon, style }),
    methods: {
      onclick: action(event)
    }
  }))
  .add('button with icon and text', () => ({
    components: { ImageButton },
    template: '<image-button :text="text" :classname="classname" @click="onclick"><i class="fas fa-code"></i></image-button>',
    data: () => ({ text, classname }),
    methods: {
      onclick: action(event)
    }
  }));;