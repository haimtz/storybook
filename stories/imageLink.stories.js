import { storiesOf } from '@storybook/vue';
import { action } from '@storybook/addon-actions';
import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all.js'

import ImageLink from '../src/components/ImageLink';

const icon = "fab fa-github-square";
const href = "/?path=/story/link--link-with-icon";
const text = "navigate to new place";
const style = {
    fontSize: "20px"
}

storiesOf('Link', module)
    .add("link with icon", () => ({
        components: { ImageLink },
        template: '<image-link :text="text" :icon="icon" :href="href" :labelStyle="style" @click="onclick"></image-link>',
        data: () => ({ text, href, icon, style }),
        methods: {
            onclick(event) {
            }
        }
    }));